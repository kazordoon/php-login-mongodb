
# php-login-mongodb
Login with PHP using MongoDB as database.

## Getting Started

- Clone the repository: `git clone https://gitlab.com/kazordoon/php-login-mongodb.git`
- Get in the project directory: `cd php-login-mongodb`

### Prerequisites
- Docker
- Docker Compose

### Run containers
- `docker-composer up -d`

### Run the script to prepare the MongoDB
- `./src/config/script.sh`

## Versioning

For the versions available, see the [tags on this repository](https://gitlab.com/kazordoon/php-login-mongodb/tags). 

## Authors

* **Felipe Barros** - *Initial work* - [kazordoon](https://gitlab.com/kazordoon)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

