<?php
session_start();

define('BASE_URL', 'http://localhost:8080/');

define('MONGODB_URI', 'mongodb://php:phppass@php_login_db:27017/php_login');
define('DB_NAME', 'php_login');

define('MAIL_HOST', '');
define('MAIL_PORT', '');
define('MAIL_USERNAME', '');
define('MAIL_PASSWORD', '');
